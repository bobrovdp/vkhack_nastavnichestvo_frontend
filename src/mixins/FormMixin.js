export default {
  props: {
    id: {
      type: String,
      default: "",
      required: true
    },
    value: { required: true },
    label: {
      type: String,
      required: true
    },
    required: {
      type: Boolean,
      default: false
    },
    config: {
      type: Object,
      default: () => {}
    }
  },
  methods: {
    update(value) {
      this.$emit("input", value);
    }
  }
};
