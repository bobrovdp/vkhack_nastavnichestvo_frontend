const mutations = {
  questionnaire(state, questionnaireData) {
    state.questionnaireData = questionnaireData;
  },
  step(state, step) {
    state.step = step;
  }
};

export { mutations };
