import axios from "axios";

const actions = {
  putStep({ commit }, questionnaire) {
    console.log(questionnaire);
    axios({
      url: "/api/questionnaires",
      data: questionnaire,
      method: "PUT"
    })
      .then(response => {
        const { questionnaire, step } = response.data;

        commit("questionnaire", questionnaire);
        commit("step", step);
      })
      .catch(err => {
        console.error(err);
      });
  },
  putQuestionnaire({ commit }, status) {
    axios({
      url: "/api/questionnaires",
      data: status,
      method: "PUT"
    })
      .then(response => {
        const { questionnaire, step } = response.data;

        commit("questionnaire", questionnaire);
        commit("step", step);
      })
      .catch(err => {
        console.error(err);
      });
  },
  getQuestionnaire({ commit }) {
    axios({
      url: "/api/questionnaires",
      method: "GET"
    })
      .then(response => {
        const { questionnaire, step } = response.data;

        commit("questionnaire", questionnaire);
        commit("step", step);
      })
      .catch(err => {
        console.error(err);
      });
  },
  setQuestionnaireData({ commit }, questionnaire) {
    commit("questionnaire", questionnaire);
  },
  setStep({ commit }, step) {
    commit("step", step);
  }
};

export { actions };
