import axios from "axios";

const actions = {
  login({ commit }, user) {
    return new Promise((resolve, reject) => {
      commit("auth_request");

      axios({
        url: "/api/auth",
        data: user,
        method: "POST"
      })
        .then(response => {
          const token = response.data.access_token;
          localStorage.setItem("token", token);
          axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
          commit("auth_success", token);
          resolve(response);
        })
        .catch(err => {
          commit("auth_error");
          localStorage.removeItem("token");
          reject(err);
        });
    });
  },
  signup({ commit }, user) {
    return new Promise((resolve, reject) => {
      commit("auth_request");

      axios({
        url: "/api/users",
        data: user,
        method: "POST"
      })
        .then(response => {
          const token = response.data.access_token;
          localStorage.setItem("token", token);
          axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
          commit("auth_success", token);
          resolve(response);
        })
        .catch(err => {
          commit("auth_error", err);
          localStorage.removeItem("token");
          reject(err);
        });
    });
  },
  logout({ commit }) {
    return new Promise(resolve => {
      commit("logout");

      localStorage.removeItem("token");
      delete axios.defaults.headers.common["Authorization"];
      resolve();
    });
  },
  getCities({ commit }) {
    axios({
      url: "/api/cities?is_works=true",
      method: "GET"
    })
      .then(response => {
        const cities = response.data.cities;
        commit("cities", cities);
      })
      .catch(err => {
        console.error(err);
      });
  }
};

export { actions };
