const state = {
  status: "",
  token: window.localStorage.getItem("token") || "",
  cities: []
};

export { state };
