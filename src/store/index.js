import Vue from "vue";
import Vuex from "vuex";
import createLogger from "vuex/dist/logger";

import auth from "./modules/auth";
import questionnaire from "./modules/questionnaire";

const dev = process.env.NODE_ENV !== "production";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    questionnaire
  },
  plugins: dev ? [createLogger] : [],
  strict: dev
});
