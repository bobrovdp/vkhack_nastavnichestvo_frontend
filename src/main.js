import Vue from "vue";
import axios from "axios";
// import Echo from 'laravel-echo';
// import Pusher from 'pusher-js';

import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";

Vue.prototype.$http = axios;
const token = localStorage.getItem("token");
if (token) {
  Vue.prototype.$http.defaults.headers.common[
    "Authorization"
  ] = `Bearer ${token}`;
}

Vue.config.productionTip = false;

// window.Pusher = Pusher;

// window.Echo = new Echo({
//   broadcaster: "pusher",
//   key: "fakekey",
//   cluster: "123",
//   wsHost: "docker.vkreview.ru",
//   wsPort: 6001
// });

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
